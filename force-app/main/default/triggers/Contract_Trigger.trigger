/**
*___________________________________________________
* @Name: Contract_Trigger
* @Author: Sofia Bramante
* @Created Date: 10/23/2020
*___________________________________________________
* @Description: This trigger defines which method to call when a new contract is created
*___________________________________________________
* @Changes:
*/

trigger Contract_Trigger on Contract__c (before insert) {
    
    Contract_TriggerHandler contractHandler = new Contract_TriggerHandler();
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            contractHandler.onBeforeInsert(Trigger.new);
        }
    }
}