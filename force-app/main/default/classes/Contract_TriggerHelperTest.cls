/**
*___________________________________________________
* @Name: Contract_TriggerHelperTest
* @Author: Sofia Bramante
* @Created Date: 10/23/2020
*___________________________________________________
* @Description: This is a test class for the helper class
*___________________________________________________
* @Changes:
*/

@isTest
public class Contract_TriggerHelperTest {

    @isTest static void testUpdateWorkDescription_listWithOneContract()
    {
        date startDate = date.TODAY();
        date endDate = startDate + 1;
        Contract__c newContract = new Contract__c(Name = 'TEST_CONTRACT_2', 
                                                  Start_Date__c = startDate, 
                                                  End_Date__c = endDate);
         insert newContract;
        
        Contract__c insertedContract = [SELECT Id, Work_Description__c FROM Contract__c WHERE Name = 'TEST_CONTRACT_2'];
        string newWorkDescription = 'This contract represents newly awarded work for TSA';
        system.assertEquals(newWorkDescription, insertedContract.Work_Description__c);
    }
    
}