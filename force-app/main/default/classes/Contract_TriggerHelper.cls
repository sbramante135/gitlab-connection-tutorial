/**
*___________________________________________________
* @Name: Contract_TriggerHelper
* @Author: Sofia Bramante
* @Created Date: 10/23/2020
*___________________________________________________
* @Description: The helper class updates the work description field whenever a new contract is created
*___________________________________________________
* @Changes:
*/

public class Contract_TriggerHelper {
    
    public static void updateContractDescription(List < Contract__c > newList) {
        string newWorkDescription = 'This contract represents newly awarded work for TSA';
        List < Contract__c > updatedContracts = new List < Contract__c >();
        for (Contract__c currentContract : newList)
        {
            currentContract.Work_Description__c = newWorkDescription;
            updatedContracts.add(currentContract);
        }
    
    update updatedContracts;    
    }
}