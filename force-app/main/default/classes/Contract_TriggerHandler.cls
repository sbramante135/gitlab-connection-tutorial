/**
*___________________________________________________
* @Name: Contract_TriggerHandler
* @Author: Sofia Bramante
* @Created Date: 10/23/2020
*___________________________________________________
* @Description: The trigger handler defines what actions should happen when a new contract is created
*___________________________________________________
* @Changes:
*/

public class Contract_TriggerHandler {
    
    public void onBeforeInsert(List<Contract__c> newList) 
    {
        system.debug('Call onbeforeInsert:');
        
        try{
            Contract_TriggerHelper.updateContractDescription(newList);
            
        }
        catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        } 
    }

}